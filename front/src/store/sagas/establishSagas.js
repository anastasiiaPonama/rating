import axiosApi from "../../axiosApi";
import {put, takeEvery} from "redux-saga/effects";
import {addNotification} from "../actions/notifierActions";

import {historyPush} from "../actions/historyActions";
import {
  createEstablishFailure, createEstablishRequest,
  createEstablishSuccess, deleteEstablishRequest, deleteEstablishSuccess,
  establishesFailure, establishesRequest,
  establishesSuccess,
  singleEstablishFailure, singleEstablishRequest,
  singleEstablishSuccess
} from "../actions/establishActions";

export function* fetchEstablishes() {
  try {
    const response = yield axiosApi.get('/establishes');
    console.log(response)
    yield put(establishesSuccess(response.data));
  } catch (e) {
    yield put(establishesFailure());
    yield put(addNotification({message: 'Could not fetch Establishes', options: {variant: 'error'}}));
  }
}

export function* fetchSingleEstablish({payload: establishId}) {
  try {
    const response = yield axiosApi.get('/establishes/' + establishId);
    yield put(singleEstablishSuccess(response.data));
  } catch (e) {
    yield put(singleEstablishFailure());
    yield put(addNotification({message: 'Fetch single establish failed', options: {variant: 'error'}}));
  }
}

export function* createNewEstablish({payload: formData}) {
  console.log(formData)
  try {
    yield axiosApi.post('/establishes', formData);
    yield put(createEstablishSuccess());
    yield put(establishesRequest());
    yield put(addNotification({message: 'New place is being moderated...', options: {variant: 'success'}}));
  } catch (e) {
    yield put(createEstablishFailure(e.response.data));
    yield put(addNotification({message: 'Create place failed', options: {variant: 'error'}}));
  }
}

export function* deleteEstablish({payload: id}) {
  try {
    yield axiosApi.delete(`/establishes/${id}`);
    yield put(deleteEstablishSuccess(id));
    yield put(historyPush('/'));
    yield put(establishesRequest());
    yield put(addNotification({message: 'Delete place successful', options: {variant: 'success'}}));
  } catch (e) {
    yield put(addNotification({message: 'Delete place failed', options: {variant: 'error'}}));
  }
}

const groupsSagas = [
  takeEvery(establishesRequest, fetchEstablishes),
  takeEvery(singleEstablishRequest, fetchSingleEstablish),
  takeEvery(createEstablishRequest, createNewEstablish),
  takeEvery(deleteEstablishRequest, deleteEstablish)

];

export default groupsSagas;