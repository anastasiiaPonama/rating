import axiosApi from "../../axiosApi";
import {put, takeEvery} from "redux-saga/effects";
import {addNotification} from "../actions/notifierActions";

import {
  establishesFailure, establishesRequest,
} from "../actions/establishActions";
import {
  createReviewFailure,
  createReviewRequest,
  createReviewSuccess, deleteReviewRequest,
  deleteReviewSuccess,
  reviewsRequest, reviewsSuccess
} from "../actions/reviewsActions";

export function* fetchReviews({payload: establishId}) {
  try {
    const response = yield axiosApi.get(`/reviews?establish=${establishId}`);
    yield put(reviewsSuccess(response.data));
  } catch (e) {
    yield put(establishesFailure());
    yield put(addNotification({message: 'Could not fetch Establishes', options: {variant: 'error'}}));
  }
}

export function* createNewReview({payload: data}) {
  try {
    yield axiosApi.post('/reviews', data);
    yield put(createReviewSuccess());
    yield put(reviewsRequest());
    yield put(addNotification({message: 'New review created successful', options: {variant: 'success'}}));
  } catch (e) {
    yield put(createReviewFailure(e.response.data));
    yield put(addNotification({message: 'Create review failed', options: {variant: 'error'}}));
  }
}

export function* deleteReview({payload: id}) {
  try {
    yield axiosApi.delete(`/reviews/${id}`);
    yield put(deleteReviewSuccess(id));
    yield put(reviewsRequest());
    yield put(establishesRequest());
    yield put(addNotification({message: 'Delete review successful', options: {variant: 'success'}}));
  } catch (e) {
    yield put(addNotification({message: 'Delete review failed', options: {variant: 'error'}}));
  }
}

const groupsSagas = [
  takeEvery(reviewsRequest, fetchReviews),
  takeEvery(createReviewRequest, createNewReview),
  takeEvery(deleteReviewRequest, deleteReview)
];

export default groupsSagas;