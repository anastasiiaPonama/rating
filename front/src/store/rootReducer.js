import {combineReducers} from "redux";
import usersSlice from "./slices/usersSlice";
import establishSlice from "./slices/establishSlice";
import reviewsSlice from "./slices/reviewsSlice";

const rootReducer = combineReducers({
    users: usersSlice.reducer,
    establishes: establishSlice.reducer,
    reviews: reviewsSlice.reducer
});

export default rootReducer;