import {createSlice} from "@reduxjs/toolkit";

export const initialState = {
  establishes: [],
  establishesLoading: false,
  singleEstablish: {},
  singleEstablishLoading: false,
  createEstablishLoading: false,
  createEstablishError: null
};

const name = 'establishes';

const establishSlice = createSlice({
  name,
  initialState,
  reducers: {
    establishesRequest: state => {
      state.establishesLoading = true;
    },
    establishesSuccess: (state, {payload: establishes}) => {
      state.establishesLoading = false;
      state.establishes = establishes;
    },
    establishesFailure: state => {
      state.establishesLoading = false;
    },
    singleEstablishRequest: state => {
      state.singleEstablishLoading = true;
    },
    singleEstablishSuccess: (state, {payload: establishes}) => {
      state.singleEstablishLoading = false;
      state.singleEstablish = establishes;
    },
    singleEstablishFailure: state => {
      state.singleEstablishLoading = false;
    },
    createEstablishRequest: state => {
      state.createEstablishLoading = true;
    },
    createEstablishSuccess: state => {
      state.createEstablishLoading = false;
    },
    createEstablishFailure: (state, {payload: error}) => {
      state.createEstablishLoading = false;
      state.createEstablishError = error;
    },
    deleteEstablishRequest: () => {},
    deleteEstablishSuccess: (state, {payload: id}) => {
      state.establishes = state.establishes.filter(c => c.id !== id);
    },
    deleteReviewRequest: () => {},
    deleteReviewSuccess: () => {},
    deleteEstablishImagesRequest: () => {},
    deleteEstablishImagesSuccess: () => {},
  }
});

export default establishSlice;