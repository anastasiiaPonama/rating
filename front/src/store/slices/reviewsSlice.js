import {createSlice} from "@reduxjs/toolkit";

export const initialState = {
  reviews: [],
  reviewsLoading: false,
  createReviewLoading: false,
  createReviewError: null
};

const name = 'reviews';

const reviewsSlice = createSlice({
  name,
  initialState,
  reducers: {
    reviewsRequest: state => {
      state.reviewsLoading = true;
    },
    reviewsSuccess: (state, {payload: reviews}) => {
      state.reviewsLoading = false;
      state.reviews = reviews;

    },
    reviewsFailure: state => {
      state.reviewsLoading = false;
    },
    createReviewRequest: state => {
      state.createReviewLoading = true;
    },
    createReviewSuccess: state => {
      state.createReviewLoading = false;
    },
    createReviewFailure: (state, {payload: error}) => {
      state.createReviewLoading = false;
      state.createReviewError = error;
    },
    deleteReviewRequest: () => {},
    deleteReviewSuccess: (state, {payload: id}) => {
      state.reviews = state.reviews.filter(c => c.id !== id);
    }
  }
});

export default reviewsSlice;