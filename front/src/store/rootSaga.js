import {all} from 'redux-saga/effects';
import history from "../history";
import historySagas from "./sagas/historySagas";
import usersSagas from "./sagas/usersSagas";
import establishSagas from "./sagas/establishSagas";
import reviewsSagas from "./sagas/reviewsSagas";

export default function* rootSaga() {
  yield all([
    ...historySagas(history),
    ...usersSagas,
    ...establishSagas,
    ...reviewsSagas
  ])
}