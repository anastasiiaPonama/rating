import establishSlice from "../slices/establishSlice";

export const {
  establishesRequest,
  establishesSuccess,
  establishesFailure,
  singleEstablishRequest,
  singleEstablishSuccess,
  singleEstablishFailure,
  createEstablishRequest,
  createEstablishSuccess,
  createEstablishFailure,
  deleteEstablishRequest,
  deleteEstablishSuccess
} = establishSlice.actions;

