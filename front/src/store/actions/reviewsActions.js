import reviewsSlice from "../slices/reviewsSlice";

export const {
  reviewsRequest,
  reviewsSuccess,
  reviewsFailure,
  createReviewRequest,
  createReviewSuccess,
  createReviewFailure,
  deleteReviewRequest,
  deleteReviewSuccess
} = reviewsSlice.actions;

