import React, {useEffect, useState} from 'react';
import Rating from '@material-ui/lab/Rating';
import makeStyles from "@material-ui/core/styles/makeStyles";
import {useDispatch, useSelector} from "react-redux";
import {singleEstablishRequest} from "../../store/actions/establishActions";
import Grid from "@material-ui/core/Grid";
import {Box, Divider, Paper, TextField, Typography} from "@material-ui/core";
import {apiURL} from "../../config";
import Button from "@material-ui/core/Button";
import {createReviewRequest, deleteReviewRequest, reviewsRequest} from "../../store/actions/reviewsActions";
import FileInput from "../../components/UI/FileInput/FileInput";
import ButtonWithProgress from "../../components/UI/ButtonWithProgress/ButtonWithProgress";

const useStyles = makeStyles(theme => ({
  root: {
    position: 'relative'
  }
}));

const SingleEstablish = ({match}) => {
  const params = match.params.id;
  const classes = useStyles();
  const dispatch = useDispatch();
  const user = useSelector(state => state.users.user);
  console.log(user)
  const singleEstablish = useSelector(state => state.establishes.singleEstablish);
  const reviews = useSelector(state => state.reviews.reviews);
  console.log(reviews)

  const [addReview, setAddReview] = useState('');
  const [value, setValue] = React.useState({
    food: 2,
    service: 2,
    interior: 2
  });
  const [photo, setPhoto] = useState({
    photo: ''
  });

  useEffect(() => {
    dispatch(singleEstablishRequest(match.params.id));
  }, [dispatch, match.params.id]);

  useEffect(() => {
    dispatch(reviewsRequest(match.params.id));
  }, [dispatch, match.params.id])

  const deleteReview = id => {
    dispatch(deleteReviewRequest(id));
  };

  const onReviewFormSubmit = (e, data, rating) => {
    e.preventDefault();
    dispatch(createReviewRequest({establish: params, review: data, ratingScale: [rating]}));
  };

  const inputChangeHandler = e => {
    const name = e.target.name;
    const value = e.target.value;

    setValue(prevState => ({
      ...prevState,
      [name]: value
    }));
  };

  const fileChangeHandler = e => {
    const name = e.target.name;
    const file = e.target.files[0];

    setPhoto(prevState => ({
      ...prevState,
      [name]: file
    }));
  };

  const submitPhotoFormHandler = e => {
    e.preventDefault();
    const formData = new FormData();
    Object.keys(photo).forEach(key => {
      formData.append(key, photo[key]);
    });

    // dispatch()
  };

  return singleEstablish ? (
    <Grid container direction="column" spacing="2">
      <Grid item xs>
        <Typography variant="h5">{singleEstablish.title}</Typography>
      </Grid>
      <Grid item xs>
        <Typography variant="body2">{singleEstablish.datetime}</Typography>
      </Grid>
      {singleEstablish.image && (
        <Grid item xs>
          <img src={apiURL + '/uploads/' + singleEstablish.image}
               alt={singleEstablish.title} style={{maxWidth: 200, maxHeight: 200}}
          />
        </Grid>
      )}
      <Grid item xs>
        <Divider/>
      </Grid>
      <Grid item xs>
        <Typography>Gallery</Typography>
      </Grid>
      <Grid item xs container direction="column" spacing={1}>
        {singleEstablish.establishImages && singleEstablish.establishImages.length === 0 ? (
          <Typography variant={"body2"}>No images</Typography>
        ) : singleEstablish.establishImages && singleEstablish.establishImages.map(image => (
          <Grid item key={image._id}>
            <Paper component={Box}>
              <img src={apiURL + '/uploads/' + image.establishImage}
                   style={{maxWidth: 200, maxHeight: 200}}
              />
            </Paper>
          </Grid>
        ))}
      </Grid>
      <Grid item xs>
        <Divider/>
      </Grid>
      <Grid item xs>
        <Typography variant="body1">{singleEstablish.description}</Typography>
      </Grid>
      <Grid item xs>
        <Divider/>
      </Grid>
      <Grid item xs>
        <Typography>Reviews</Typography>
      </Grid>
      <Grid item xs container direction="column" spacing={1}>
        {reviews && reviews.length === 0 ? (
          <Typography variant={"body2"}>No reviews</Typography>
        ) : reviews && reviews.map(review => (
          <Grid item key={review._id}>
            <Paper component={Box} p={2} direction="column" justify='center'>
              <Typography><b>On: </b> {review.datetime}, <b>{review.user.displayName}</b> said:</Typography>
              <Typography>{review.review}</Typography>
              {review.ratingScale && review.ratingScale.map(rating => (
                <Grid container direction='column' alignItems='center' spacing={2} mt={2}>
                  <Grid container direction='row' alignItems='center' spacing={2}>
                    <Grid item>
                      <Typography>Quality of food:  </Typography>
                    </Grid>
                    <Grid item>
                      <Rating
                        name="food"
                        value={rating.food}
                        readOnly
                      />
                    </Grid>
                  </Grid>
                  <Grid container direction='row' alignItems='center' spacing={2}>
                    <Grid item>
                      <Typography>Quality of service:  </Typography>
                    </Grid>
                    <Grid item>
                      <Rating
                        name="service"
                        value={rating.service}
                        readOnly
                      />
                    </Grid>
                  </Grid>
                  <Grid container direction='row' alignItems='center' spacing={2}>
                    <Grid item>
                      <Typography>Quality of interior:  </Typography>
                    </Grid>
                    <Grid item>
                      <Rating
                        name="interior"
                        value={rating.interior}
                        readOnly
                      />
                    </Grid>
                  </Grid>
                </Grid>
              ))}
            </Paper>
            {user.user.role === "admin" && (
              <Button onClick={() => dispatch(deleteReview(review._id))}>Delete review</Button>
            )}
          </Grid>
        ))}
      </Grid>
      <Grid item xs>
        <Divider/>
      </Grid>
      <Grid container direction="column" spacing={2} component="form" onSubmit={(e) => onReviewFormSubmit(e, addReview, value)}>
        <Grid item xs>
          <Typography variant="h6">Add new review</Typography>
        </Grid>
        <Grid item xs>
          <TextField
            fullWidth
            variant="outlined"
            label="Review"
            value={addReview}
            onChange={e => setAddReview(e.target.value)}
          />
        </Grid>
        <Grid container item xs>
          <Box component="fieldset" borderColor="transparent">
            <Typography component="legend">Quality of food</Typography>
            <Rating
              name="food"
              value={value.food}
              onChange={inputChangeHandler}
            />
          </Box>
          <Box component="fieldset" borderColor="transparent">
            <Typography component="legend">Quality of service</Typography>
            <Rating
              name="service"
              value={value.service}
              onChange={inputChangeHandler}
            />
          </Box>
          <Box component="fieldset" borderColor="transparent">
            <Typography component="legend">Quality of interior</Typography>
            <Rating
              label="Interior"
              name="interior"
              value={value.interior}
              onChange={inputChangeHandler}
            />
          </Box>
        </Grid>
        <Grid item xs>
          <Button
            variant="contained"
            color="primary"
            type="submit"
          >
            Send review
          </Button>
        </Grid>
      </Grid>
      <Grid item xs>
        <form onSubmit={submitPhotoFormHandler} noValidate>
          <Grid container direction="column" spacing={2}>
            <Grid item xs>
              <FileInput
                name="photo"
                label="Choose file"
                onChange={fileChangeHandler}
              />
            </Grid>

            <Grid item xs>
              <ButtonWithProgress
                type="submit" color="primary" variant="contained"
                // loading={loading} disabled={loading}
              >
                Upload
              </ButtonWithProgress>
            </Grid>
          </Grid>
        </form>
      </Grid>
    </Grid>
  ) : null;
};

export default SingleEstablish;