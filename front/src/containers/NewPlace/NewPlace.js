import React, {useState} from 'react';
import {useDispatch, useSelector} from "react-redux";
import {
  FormControl,
  FormControlLabel,
  FormLabel,
  Radio,
  RadioGroup,
  Typography
} from "@material-ui/core";
import Grid from "@material-ui/core/Grid";
import FormElement from "../../components/UI/Form/FormElement";
import FileInput from "../../components/UI/FileInput/FileInput";
import ButtonWithProgress from "../../components/UI/ButtonWithProgress/ButtonWithProgress";
import {createEstablishRequest} from "../../store/actions/establishActions";

const NewPlace = () => {
  const dispatch = useDispatch();
  const error = useSelector(state => state.establishes.createEstablishError);
  const loading = useSelector(state => state.establishes.createEstablishLoading);

  const [state, setState] = useState({
    title: '',
    description: '',
    image: '',
    onAgree: false
  });

  const inputChangeHandler = e => {
    const name = e.target.name;
    const value = e.target.value;

    setState(prevState => ({
      ...prevState,
      [name]: value
    }));
  };

  const submitFormHandler = e => {
    e.preventDefault();

    const formData = new FormData();

    Object.keys(state).forEach(key => {

      formData.append(key, state[key]);
    });

    dispatch(createEstablishRequest(formData));
  };

  const fileChangeHandler = e => {
    const name = e.target.name;
    const file = e.target.files[0];

    setState(prevState => ({
      ...prevState,
      [name]: file
    }));
  };

  const getFieldError = fieldName => {
    try {
      return error.errors[fieldName].message;
    } catch (e) {
      return undefined;
    }
  };

  const onRadioClickHandler = () => {
    setState(prevState => ({
      ...prevState,
      onAgree: true
    }));
  }

  return (
    <Grid container direction="column" spacing={2}>
      <Grid item xs>
        <Typography variant="h4">Add new place</Typography>
      </Grid>
      <Grid item xs>
        <form onSubmit={submitFormHandler} noValidate>
          <Grid container direction="column" spacing={2}>
            <FormElement
              required
              label="Title"
              name="title"
              value={state.title}
              onChange={inputChangeHandler}
              error={getFieldError('title')}
            />

            <FormElement
              multiline
              rows={3}
              label="Description"
              name="description"
              value={state.description}
              onChange={inputChangeHandler}
            />

            <Grid item xs>
              <FileInput
                name="image"
                label="Image"
                onChange={fileChangeHandler}
                error={getFieldError('image')}
              />
            </Grid>

            <Grid item xs>
              <FormControl required component="fieldset">
                <FormLabel component="legend">By submitting this form, you agree that the following information will be submitted to the public domain,
                  and administrators of this site will have full control over the said information</FormLabel>
                <RadioGroup aria-label="gender" name="onAgree" value={state.onAgree} onChange={inputChangeHandler}>
                  <FormControlLabel value="onAgree" control={<Radio />} label="I understand" />
                </RadioGroup>
              </FormControl>
            </Grid>

            <Grid item xs>
              <ButtonWithProgress
                type="submit" color="primary" variant="contained"
                loading={loading} disabled={loading}
              >
                Submit new place
              </ButtonWithProgress>
            </Grid>
          </Grid>
        </form>
      </Grid>
    </Grid>
  );
};

export default NewPlace;