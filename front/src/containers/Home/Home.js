import React, {useEffect, useState} from 'react';
import {Link} from "react-router-dom";
import Grid from "@material-ui/core/Grid";
import Button from "@material-ui/core/Button";
import {useDispatch, useSelector} from "react-redux";
import {establishesRequest} from "../../store/actions/establishActions";
import makeStyles from "@material-ui/core/styles/makeStyles";
import {Box, CircularProgress, Paper, Typography} from "@material-ui/core";
import {apiURL} from "../../config";

const useStyles = makeStyles((theme) => ({
  progress: {
    height: 200
  }
}));

const Home = () => {
  const classes = useStyles();
  const dispatch = useDispatch();
  const user = useSelector(state => state.users.user);
  const establishes = useSelector(state => state.establishes.establishes);
  console.log(establishes)
  const loading = useSelector(state => state.establishes.createEstablishLoading);

  useEffect(() => {
    dispatch(establishesRequest());
  }, [dispatch]);

  return (
    <Grid item container spacing={6} direction="column">
      <Grid item container justify="space-between" alignItems="center" spacing={3}>
        <Grid item>
          <Typography variant="h4">All places</Typography>
        </Grid>

        {user && (
          <Grid item>
            <Button color="primary" component={Link} to="/places-new">Add new place</Button>
          </Grid>
        )}

        <Grid item container spacing={4}>
          {loading ? (
            <Grid container justify="center" alignItems="center" className={classes.progress}>
              <Grid item>
                <CircularProgress/>
              </Grid>
            </Grid>
          ) : establishes && establishes.map(establish => (
            <Grid item xs key={establish._id}>
              <Paper component={Box} p={2}>
                <Grid container spacing={2} alignItems="center">
                  {establish.image && (
                    <Grid item>
                      <img src={apiURL + '/uploads/' + establish.image}
                           alt={establish.title} style={{maxWidth: 200, maxHeight: 200}}
                      />
                    </Grid>
                  )}
                  <Grid item container direction='column'>
                    <Grid item>
                      <Typography>{establish.title}</Typography>
                    </Grid>
                    <Grid item>
                      <Typography>({establish.reviews.length} - reviews)</Typography>
                    </Grid>
                    <Grid item>
                      <Typography>({establish.establishImages.length} - photos)</Typography>
                    </Grid>
                  </Grid>
                  {user && (
                    <Grid item>
                      <Button component={Link} to={"/establishes/" + establish._id}>View</Button>
                    </Grid>
                  )}
                </Grid>
              </Paper>
            </Grid>
          ))}
        </Grid>
      </Grid>
    </Grid>
  );
};

export default Home;