import React from 'react';
import {Switch, Route} from "react-router-dom";

import AppToolbar from "./components/UI/AppToolbar/AppToolbar";
import CssBaseline from "@material-ui/core/CssBaseline";
import Container from "@material-ui/core/Container";
import Register from "./containers/Register/Register";
import Login from "./containers/Login/Login";
import Home from "./containers/Home/Home";
import SingleEstablish from "./containers/SingleEstablish/SingleEstablish";
import NewPlace from "./containers/NewPlace/NewPlace";

const App = () => (
  <>
    <CssBaseline/>
    <header>
      <AppToolbar/>
    </header>
    <main>
      <Container maxWidth="xl">
        <Switch>
          <Route path="/" exact component={Home}/>
          <Route path="/establishes/:id" exact component={SingleEstablish}/>
          <Route path="/places-new" exact component={NewPlace}/>
          <Route path="/register" exact component={Register}/>
          <Route path="/login" exact component={Login}/>
        </Switch>
      </Container>
    </main>
  </>
);

export default App;
