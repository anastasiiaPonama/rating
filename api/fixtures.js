const mongoose = require('mongoose');
const config = require('./config');
const {nanoid} = require('nanoid');

const User = require("./models/User");
const Establish = require("./models/Establish");
const Reviews = require("./models/Reviews");
const EstablishImages = require("./models/EstablishImages");

const run = async () => {
    await mongoose.connect(config.db.url, config.db.options);

    const collections = await mongoose.connection.db.listCollections().toArray();

    for (const coll of collections) {
        await mongoose.connection.db.dropCollection(coll.name);
    }

    const [User1, Admin] = await User.create({
        email: 'user@test',
        password: '123',
        token: nanoid(),
        role: 'user',
        displayName: 'User'
    }, {
        email: 'admin@test',
        password: '123',
        token: nanoid(),
        role: 'admin',
        displayName: 'Admin'
    });

    const [FoodZone, Restaurant] = await Establish.create({
        user: User1._id,
        title: 'FoodZone',
        description: 'Some description',
        image: 'fixtures/group-icon.jpeg'
    }, {
        user: User1._id,
        title: 'Restaurant',
        description: 'Some description',
        image: 'fixtures/group-icon2.webp'
    });

    const [review1, review2, review3] = await Reviews.create({
        user: User1._id,
        establish: FoodZone._id,
        review: 'Some text for review',
        ratingScale: [{
            "food": 3,
            "service": 5,
            "interior": 4
        }],
        published: true
    }, {
        user: User1._id,
        establish: FoodZone._id,
        review: 'Some review for FoodZone space',
        ratingScale: [{
            "food": 5,
            "service": 5,
            "interior": 4
        }],
        published: true
    }, {
        user: Admin._id,
        establish: FoodZone._id,
        review: 'Some review for Restaurant space',
        ratingScale: [{
            "food": 5,
            "service": 3,
            "interior": 4
        }],
        published: true
    });

    const [image1, image2, image3] = await EstablishImages.create({
        user: Admin._id,
        establish: FoodZone._id,
        establishImage: 'fixtures/group-icon2.webp'
    }, {
        user: Admin._id,
        establish: FoodZone._id,
        establishImage: 'fixtures/group-icon2.webp'
    }, {
        user: User1._id,
        establish: Restaurant._id,
        establishImage: 'fixtures/group-icon2.webp'
    });

    FoodZone.establishImages = [image1, image2];
    FoodZone.reviews = [review1, review2];
    Restaurant.establishImages = [image3];
    Restaurant.reviews = [review3];

    await mongoose.connection.close();
};

run().catch(console.error);