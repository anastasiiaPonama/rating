const express = require('express');
const auth = require("../middleware/auth");
const permit = require("../middleware/permit");
const upload = require('../multer').image;

const EstablishImages = require('../models/EstablishImages');
const Establish = require('../models/Establish');

const router = express.Router();

router.get('/', async (req, res) => {
  try {
    const criteria = {};

    if(req.query.establish) {
      criteria.establish = req.query.establish;
    }

    const establishesImg = await EstablishImages.find(criteria).populate('establish');

    res.send(establishesImg);
  } catch (e) {
    res.sendStatus(500);
  }
});

router.post('/', auth, permit('user', 'admin'), upload.single('establishImage'), async (req, res) => {
  try {
    const imageData = req.body;
    imageData.user = req.user._id;
    imageData.establish = req.body.establish;

    if (req.file) {
      imageData.establishImage = req.file.filename;
    }

    const establishImg = new EstablishImages(imageData);
    await establishImg.save();

    if (establishImg) {
      const establish = await Establish.findOne({_id: req.body.establish});
      establish.establishImages.push(establishImg);

      await establish.save();
      res.send({message: 'Success', establishImg});
    }

    if (!establishImg) {
      res.send({message: 'Establish not found!'});
    }
  } catch (e) {
    res.status(400).send(e);
  }
});

module.exports = router;