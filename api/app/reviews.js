const express = require('express');
const auth = require("../middleware/auth");
const permit = require("../middleware/permit");

const Reviews = require('../models/Reviews');
const Establish = require('../models/Establish');

const router = express.Router();

router.get('/', async (req, res) => {
  try {
    const criteria = {};

    if(req.query.establish) {
      criteria.establish = req.query.establish;
    }

    const reviews = await Reviews.find(criteria).populate('establish').populate('user');

    res.send(reviews);
  } catch (e) {
    res.sendStatus(500);
  }
});

router.post('/', auth, permit('user', 'admin'), async (req, res) => {
  try {
    const reviewsData = req.body;
    reviewsData.user = req.user._id;
    reviewsData.establish = req.body.establish;
    reviewsData.ratingScale = req.body.ratingScale;

    const review = new Reviews(reviewsData);
    await review.save();

    if (review) {
      const establish = await Establish.findOne({_id: req.body.establish});
      establish.reviews.push(review);

      await establish.save();
      res.send({message: 'Your review is being moderated...', review});
    }

    if (!review) {
      res.send({message: 'Establish not found!'});
    }
  } catch (e) {
    res.status(400).send(e);
  }
});

router.get('/reviews', auth, permit('user', 'admin'), async (req, res) => {
  try {
    const criteria = {};

    if(req.query.establish) {
      criteria.establish = req.query.establish;
    }

    const reviews = await Reviews.find(criteria).populate('ratingScale');

  } catch (e) {
    res.status(400).send(e);
  }
});

module.exports = router;
