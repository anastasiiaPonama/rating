const express = require('express');
const auth = require("../middleware/auth");
const permit = require("../middleware/permit");
const upload = require('../multer').image;

const Establish = require('../models/Establish');

const router = express.Router();

router.get('/', async (req, res) => {
  try {
    const establishes = await Establish.find();

    res.send(establishes);
  } catch (e) {
    res.sendStatus(500);
  }
});

router.get('/:id', async (req, res) => {
  try {
    const establish = await Establish.findOne({_id: req.params.id});

    if (establish) {
      res.send(establish)
    } else (
      res.sendStatus(404)
    )
  } catch (e) {
    res.sendStatus(500);
  }
});

router.post('/', upload.single('image'), async (req, res) => {
  try {
    console.log(req.body)
    const establishData = req.body;
    establishData.user = req.user._id;

    if (req.file) {
      establishData.image = req.file.filename;
    }

    const establish = new Establish(establishData);
    await establish.save();

    res.send({message: 'Your new place is being moderated...', establish});
  } catch (e) {
    res.status(400).send(e);
  }
});

router.post('/:id', auth, permit('admin'), async (req, res) => {
  try {
    const establish = await Establish.findById(req.params.id);
    establish.published = true;
    await establish.save();

    res.send(establish);
  } catch (e) {
    res.status(400).send(e);
  }
});

router.delete('/:id', auth, permit('admin'), async (req, res) => {
  try {
    const establishReq = req.body;
    establishReq.user = req.user._id;

    if (establishReq.user) {
      const establish = await Establish.findOne({_id: req.params.id}).remove();
      res.send(establish);
    } else {
      res.sendStatus(403);
    }

  } catch (e) {
    res.sendStatus(500);
  }
});

router.delete('/:id/:reviewId', auth, permit('admin'), async (req, res) => {
  try {
    const establishReq = req.body;
    establishReq.user = req.user._id;

    if (establishReq.user) {
      const establish = await Establish.update({_id: req.params.id}, {$pull: {reviews: req.params.reviewId}})
      res.send(establish);
    } else {
      res.sendStatus(403);
    }

  } catch (e) {
    res.sendStatus(500);
  }
});

router.delete('/:id/:establishImages', auth, permit('admin'), async (req, res) => {
  try {
    const establishReq = req.body;
    establishReq.user = req.user._id;

    if (establishReq.user) {
      const establish = await Establish.update({_id: req.params.id}, {$pull: {establishImages: req.params.establishImages}})
      res.send(establish);
    } else {
      res.sendStatus(403);
    }

  } catch (e) {
    res.sendStatus(500);
  }
});

module.exports = router;