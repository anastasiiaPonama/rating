const mongoose = require('mongoose');

const RatingSchema = new mongoose.Schema({
    food: {
      type: Number,
      min: 1,
      max: 5,
      required: true
    },
    service: {
      type: Number,
      min: 1,
      max: 5,
      required: true
    },
    interior: {
      type: Number,
      min: 1,
      max: 5,
      required: true
    }
});

const ReviewsSchema = new mongoose.Schema({
  user: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'User',
    required: true
  },
  establish: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Establish',
    required: true
  },
  review: {
    type: String,
  },
  ratingScale: [RatingSchema],
  datetime: {
    type: String,
    default: new Date().toISOString()
  },
  published: {
    type: Boolean,
    default: false
  }
});

const Reviews = mongoose.model('Reviews', ReviewsSchema);

module.exports = Reviews;