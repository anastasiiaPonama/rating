const mongoose = require('mongoose');

const EstablishSchema = new mongoose.Schema({
  user: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'User',
    required: true
  },
  title: {
    type: String
  },
  description: {
    type: String
  },
  image: {
    type: String
  },
  establishImages: [{
    type: mongoose.Schema.Types.ObjectId,
    ref: 'EstablishImages',
  }],
  ratings: [{
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Ratings',
  }],
  reviews: [{
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Reviews'
  }],
  datetime: {
    type: String,
    default: new Date().toISOString()
  }
});

const Establish = mongoose.model('Establish', EstablishSchema);

module.exports = Establish;