const mongoose = require('mongoose');

const EstablishImagesSchema = new mongoose.Schema({
  user: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'User',
    required: true
  },
  establish: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Establish',
    required: true
  },
  establishImage: {
    type: String,
    required: true
  },
  datetime: {
    type: String,
    default: new Date().toISOString()
  }
});

const EstablishImages = mongoose.model('EstablishImages', EstablishImagesSchema);

module.exports = EstablishImages;